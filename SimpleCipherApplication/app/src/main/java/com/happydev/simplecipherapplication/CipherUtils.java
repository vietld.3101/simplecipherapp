package com.happydev.simplecipherapplication;

import android.util.Log;

public class CipherUtils {
    public static int charToIndex(char input) throws Exception {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        return alphabet.indexOf(input) + 1;
    }

    public static char indexToChar(int index) throws Exception {
        Log.d("asdasda", index + " ");
        if(index > 26){
            index = index - 26;
        }
        if(index < 1){
            index = index + 26;
        }
        final String alphabet = "abcdefghijklmnopqrstuvwxyz";
        return alphabet.charAt(index - 1);
    }
}
