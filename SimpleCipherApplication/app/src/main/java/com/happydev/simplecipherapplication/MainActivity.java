package com.happydev.simplecipherapplication;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private EditText edInput;
    private TextView tvOutput;
    private EditText edKey;
    private Button btnEncrypt;
    private Button btnDecrypt;

    private int key = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        edKey.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    key = Integer.parseInt((edKey.getText().toString()));
                    Log.d(getPackageName(), key + " ");
                } catch (Exception e){
                    e.printStackTrace();
                    key = 1;
                }
            }
        });

        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();

                String text = edInput.getText().toString();
                StringBuilder outputText = new StringBuilder();

                for(int i = 0; i < text.length(); i++){
                    try {
                        int index = CipherUtils.charToIndex(text.charAt(i));
                        outputText.append(CipherUtils.indexToChar(index + key));

                    } catch (Exception e) {
                        e.printStackTrace();
                        tvOutput.setText(e.toString());
                        break;
                    }
                }
                tvOutput.setText(outputText.toString());
            }
        });

        btnDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();

                String text = edInput.getText().toString().toLowerCase();
                StringBuilder outputText = new StringBuilder();

                for(int i = 0; i < text.length(); i++){
                    try {
                        int index = CipherUtils.charToIndex(text.charAt(i));
                        outputText.append(CipherUtils.indexToChar(index - key));

                    } catch (Exception e) {
                        e.printStackTrace();
                        tvOutput.setText(e.toString());
                        break;
                    }
                }
                tvOutput.setText(outputText.toString());
            }
        });
    }

    private void hideKeyboard(){
        View view = getCurrentFocus();
        if(view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void initView() {
        edInput = findViewById(R.id.ed_input);
        tvOutput = findViewById(R.id.tv_output);
        edKey = findViewById(R.id.ed_key);
        btnDecrypt = findViewById(R.id.btn_decrypt);
        btnEncrypt = findViewById(R.id.btn_encrypt);
    }
}
